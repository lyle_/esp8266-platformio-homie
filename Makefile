.PHONY: all build clean upload

all: build

build: ## Compile the application
	platformio run

clean: ## Clean up temporary files
	platformio run --target clean

upload: ## Upload firmware to a target
	platformio run --target upload

uploadfs: ## Upload SPIFFS filesystem to a target
	platformio run --target uploadfs

monitor: ## Monitor serial output from device
	platformio device monitor

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@printf "\nRun 'pio --help' for PlatformIO-specific help\n"
