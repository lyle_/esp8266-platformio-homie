# ESP8266 Base Project with Homie and PlatformIO

This is a simple, self-contained project to get started programming
ESP8266 boards using [PlatformIO Core](https://docs.platformio.org/en/latest/core.html),
a handy CLI development tool which handles compilation and dependencies in a declarative
manner. In my opinion the workflow is much more elegant and modular than relying
on the Arduino IDE (makes it simple to start playing with Arduino, but the
point-and-click driven workflow and poor dependency management gets tiresome),
`arduino-cli` (relies too heavily on Arduino IDE-based manual dependency
management), or the Visual Studio Code-based [PlatformIO IDE](https://platformio.org/platformio-ide)
(friends don't let friends use Electron).

The [Homie](https://homieiot.github.io/) convention makes interacting with IoT
devices much more manageable and so this project uses the excellent
[homie-esp8266](https://github.com/homieiot/homie-esp8266/) implementation, which
greatly simplifies the amount of code you have to write to manage WiFi and
[MQTT](http://mqtt.org/) connections.

## Prerequisites

* `make` for streamlining the build process
* [Platform IO Core (CLI)](https://docs.platformio.org/en/latest/installation.html)
* Install the ESP8266 framework for Homie: `platformio lib install 555`

## Running

The Makefile includes the common operations you'll need. Run `make help` for a list.

Let's get the code running on your device. Plug in the device and follow along;
if you run into any snags, the essence of the procedure follows the
[Homie for ESP8266 Quickstart](https://homieiot.github.io/homie-esp8266/docs/develop/quickstart/getting-started/#1b-with-platformio).

	# Compile sources (this will also pull in the needed dependencies)
	make build

	# Upload the firmware to the device
	make upload

The firmware will be flashed and the device will reboot into `configuration` mode;
see the aforementioned Quickstart for details on booting into `configuration`,
`normal`, or `standalone` mode, but in essence the `configuration` mode boots up
and launches its own WiFi Access Point (named something like `Homie-c631f278df44`).

To configure the device in this mode, you connect to this AP and POST the configuration
to its [HTTP JSON API](https://homieiot.github.io/homie-esp8266/docs/develop/configuration/http-json-api/).
And you have to do it each time the device starts up. Let's have our
configuration live on the device itself.

## Flashing configuration onto the device filesystem

I find the default `configuration` mode a bit clunky to work with, and prefer to
instead flash the configuration file onto the device using a SPIFFS filesystem.
See [Uploading files to file system SPIFFS](https://docs.platformio.org/en/latest/platforms/espressif8266.html#uploading-files-to-file-system-spiffs)
for details, but in short you just need to perform a couple of steps:

* Create a `data/homie` directory in the project which the PlatformIO toolchain
will turn into a SPIFFS filesystem: `mkdir -p data/homie`.
* Copy the sample configuration file into that directory: `cp config.json data/homie`.
* Change the `name`, `device_id`, `wifi.*`, and `mqtt.*` values to match your WiFi
network and MQTT broker (I recommend signing up for a free account at [shiftr.io](shiftr.io)).
* Flash the SPIFFS image: `make uploadfs`
